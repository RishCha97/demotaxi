import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Locations {
data:any;
  constructor(public http: Http) {}
/**
 * load funtion 
 * used for loading data from .json file
 */
  load(){
    if(this.data){
      return Promise.resolve(this.data);                //if data is already loaded
    }
    return new Promise(resolve =>{

      this.http.get('assets/data/locations.json').map(res=>res.json()).subscribe(data=>{        //map function converts JSON string to JAVASCRIPT object by mapping the response and calling .json() method
        this.data = data.locations
        resolve(this.data);
      });
    });
  }
  /**
   * applyHaversine function
   * to use H() function only
   * finding its value in getDistanceBetweenPoints function
   */
  applyHaversine(locations){
    let usersLocation={
      lat: 29.3461,
      lng:79.5519
    };
    locations.map((location) =>{
      let placeLocation={
        lat: location.latitude,
        lng: location.longitude
      };
      location.distance = this.getDistanceBetweenPoints(
        usersLocation,
        placeLocation,
        'miles'
      ).toFixed(2);
    });
    return locations;
  }
  /**
   * to get Distance between to points using the formula og haversine method
   * 
   */

  getDistanceBetweenPoints(start, end, units){
    let earthRadius ={
      miles:3958.8,
      km: 6371
    };
    let R = earthRadius[units || 'miles'];
    let lat1 = start.lat;
    let lat2 = end.lat;
    let lon1 = start.lng;
    let lon2 = end.lng;

    let dLat = this.toRad((lat2-lat1));
    let dLon = this.toRad((lon2-lon1));
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
    Math.sin(dLon/2)* Math.sin(dLon/2);

    let c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R*c;

    return d;
  }
  /**
   * toRad function
   * for finding radians from degree inputs
   */

  toRad(x){
    return x*Math.PI/180;
  }
}
