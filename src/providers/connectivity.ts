import { Injectable } from '@angular/core';
import { Network } from 'ionic-native';
import { Platform }  from 'ionic-angular';
declare var Connection;

@Injectable()
export class Connectivity {
  onDevice: boolean;
  constructor(public Platform:Platform) {
    console.log('Checking Platform isCordova inside Constructor Connectivity');
    this.onDevice = this.Platform.is('cordova');      //checking whether platform is a real device or not
  }

/**
 * the isOnline Function
 * checks Whether connected to internet or not
 * Using Network Native Functionality
 */
isOnline():boolean {
  if(this.onDevice && Network.connection){
    return Network.connection !== Connection.NONE;          //returns false if 
  }
  else{
    return navigator.onLine;
  }
}
/**
 * isOffline Function for
 */
isOffline(): boolean{
  if(this.onDevice && Network.connection){
    return Network.connection === Connection.NONE;        //returns false if
  }
  else{
    return !navigator.onLine;
  }
}
}
