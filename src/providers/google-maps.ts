import { Injectable } from '@angular/core';
import { Connectivity } from './connectivity';
import { Geolocation } from 'ionic-native';
declare var google;

@Injectable()
export class GoogleMaps {
  mapElement:any;                               //used for
  pleaseConnect:any;                            //used for  
  map:any;                                      //used for
  mapInitialised: boolean =false;               //used for
  mapLoaded:any;                                //used for
  mapLoadObserver:any;                          //used for
  markers:any=[];                               //used for
  apiKey:string="AIzaSyAv0D4PSM6xx29nysdrODaO7Rd5ZSLWN50";                               
  
  
  constructor(public connectivityService: Connectivity) { }

  /**
   * the init function
   * initialization only 
   * assigning values
   */
init(mapElement: any, pleaseConnect:any):Promise<any>{
  
  this.mapElement = mapElement;                 //assigning member variable to the value passed to function
  this.pleaseConnect= pleaseConnect;

  console.log('Functin init inside google-maps');
  return this.loadGoogleMaps();
}

/**
 * loadGoogleMaps function
 * 
 */

loadGoogleMaps() : Promise<any>{
return new Promise((resolve)=>{ 
  if(typeof google == "undefined" || typeof google.maps == "undefined"){
     console.log("Google maps Javascript not yet loaded .");
     this.disableMap();
     
     
     if(this.connectivityService.isOnline()){

       window['mapInit']= ()=>{
      
         this.initMap().then(()=>{
      
           resolve(true);
      
       });
    
         this.enableMap();
       }

       let script = document.createElement("script");
       script.id = "googleMaps";

       if(this.apiKey){
         script.src ='http://maps.google.com/maps/api/js?key='+this.apiKey +'&callback=mapInit';
       }
       else{
         script.src ='http://maps.google.com/maps/api/js?callback=mapInit';
       }

       document.body.appendChild(script);
     }
  }
  else{
    if(this.connectivityService.isOnline()){
      console.log('Initializing map in google-maps.ts');
      this.initMap();
      console.log('Enabling Map in google-maps.ts');
      this.enableMap();             //if connection is there Enable map Service
    }
    else{
      console.log('disabling Map Service');
      this.disableMap();          //if Connection not find disable map service
    }
  }
  this.addConnectivityListeners();
  });
}

/**
 * the initMap Function 
 * initializes the map functionality
 */

initMap() : Promise<any>{
  this.mapInitialised = true;
  return new Promise((resolve) =>{
    Geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions={
        center: latLng,
        zoom:15,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(this.mapElement, mapOptions);
      resolve(true);
    });
  });
}
 
 /**
  * disableMap Function
  * for disabling the service when not connected to the internet
  */

 disableMap(): void{
   if(this.pleaseConnect){
     this.pleaseConnect.style.display ="block";
   }

 }

 /**
  * the enableMap Function 
  * enables map Services on getting connected to internet
  */

  enableMap():void{
    if(this.pleaseConnect){
      this.pleaseConnect.style.display ="none";
    }
  }

 /**
  * addConnectivityListeners Function
  */
  addConnectivityListeners(): void{

    document.addEventListener('online',()=>{
      console.log('online');

      setTimeout(()=>{
        if(typeof google ==="undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        }
        else{
          if(!this.mapInitialised){
            this.initMap();
          }
          this.enableMap();
        }
      },2000);
    },false);
  }

  /**
   * addMarker Fuction 
   * putting location markers at desired places
   */

  addMarker(lat: number, lng: number): void{
    let latLng = new google.maps.LatLng(lat,lng);
    
    let marker = new google.maps.Marker({
      map:this.map,
      animation:google.maps.Animation.DROP,
      position:latLng
    });
    this.markers.push(marker);
  }
}
