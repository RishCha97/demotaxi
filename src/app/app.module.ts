import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {AddPage} from '../pages/add/add';
import {ChatPage} from '../pages/chat/chat';
import {TaxiData} from '../providers/taxi-data';
import {DataService} from '../providers/data-service';
import {TaxiListPage} from '../pages/taxi-list/taxi-list';
import { MapPage } from '../pages/map/map';
import { Locations } from '../providers/locations';
import { GoogleMaps } from '../providers/google-maps';
import { Connectivity } from '../providers/connectivity';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPage,
    TaxiListPage,
    ChatPage,
    MapPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPage,
    TaxiListPage,
    ChatPage,
    MapPage
  ],
  providers: [TaxiData , DataService, Locations, GoogleMaps , Connectivity]
})
export class AppModule {}
