import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
//import {AddPage} from '../add/add';
import {TaxiListPage} from '../taxi-list/taxi-list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public cityList = [];
  public data = {
    name : 'Delhi'
    ,country : 'India'
    //,img : 'img/Delhi.jpeg'
  };
  public data2 = {
    name : 'Mumbai'
    ,country : 'India'
    //,img : 'img/Mumbai.jpeg'
  }
  // Constructor function
  constructor(public navController: NavController,
    public modalCtrl: ModalController) {
    this.navController = navController;
    this.cityList.push(this.data);
    this.cityList.push(this.data2);
  }

  viewTaxiList(cityData) {
    console.log('View Taxi List');
    this.navController.push(TaxiListPage, { cityData: cityData });
  }
}
