import { Component, ViewChild } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { DataService } from '../../providers/data-service';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage {

  @ViewChild('chat') chat: any;
  public taxi;
  chatMessage: string = '';
  messages: any = [];
  image :any;
  username : string = 'anonymous';
  name : string = 'Nikhil';
  location : string = 'Nikhil';
  city : string = 'Nikhil';

  // constructor for the class 
  constructor(public navCtrl: NavController, 
  public navParams: NavParams,
  public dataService: DataService) {
      this.taxi = navParams.get('taxi');
      console.log('Taxi name is :'+ this.taxi.name);
      this.name = this.taxi.name;
      this.location = this.taxi.location;
      this.city = this.taxi.city;
      this.username = 'User 1';
      this.image = '../../assets/images/profile/200x200messi.png'
    this.dataService.getDocuments().then((data) => {
      this.messages = data;
      this.chat.scrollToBottom();
    });
  }

  // method to send messages
  sendMessage(): void {
    let message = {
      '_id': new Date().toJSON(),
      'username': this.username,
      'toname' : this.name,
      'tocity' : this.city,
      'tolocation' : this.location,
       'image' : this.image,
      'message': this.chatMessage
    };
    this.dataService.addDocument(message);
    this.chatMessage = '';

  }
}
