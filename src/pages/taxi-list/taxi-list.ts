import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import {TaxiData} from '../../providers/taxi-data';
import {ChatPage} from '../chat/chat';
import { MapPage } from '../map/map';

@Component({
  selector: 'page-taxi-list',
  templateUrl: 'taxi-list.html'
})
export class TaxiListPage {
  public cityData;
  taxiList: Array<{ name: string, city: string, id: string,location :string , image :any}> = [];
  //public taxiList = [];
  constructor(public navController: NavController,
    public navParams: NavParams,
    public taxiData: TaxiData) {
    this.navController = navController;
    this.cityData = navParams.get('cityData');
    this.taxiData.getTaxis().then((taxis = []) => {
      taxis.forEach(taxi => {
        if (taxi.city == this.cityData.name) {
          this.taxiList.push({
            name: taxi.name,
            city: taxi.city,
            id: taxi.id,
            location: taxi.location,
            image: taxi.image
          });
        }
      });
    });
  }

  
  ionViewDidLoad() {
    console.log('Hello TaxiList Page');
  }

   // View Forecast for 7 days
  doChat(taxi) {
    console.log('View Taxi List');
    this.navController.push(ChatPage,{ taxi: taxi });
  }
  viewMap(){
    this.navController.push(MapPage);
  }
}

